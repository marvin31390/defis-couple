import data from '../components/data/data.json'

function Defis() {
  const alea = Math.floor(Math.random() * 17);
    return (
      <div className="defis">
        <h1 className="title">{data.Défis[alea].Durée}</h1>
        <p className="phrase">{data.Défis[alea].Description}</p>          
        <p className="emoji">{data.Défis[alea].Emoji}</p>          
      </div>
    );
  }


  export default Defis