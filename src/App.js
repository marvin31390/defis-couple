import logo from './logo_defis.png';
import './App.css';
import Defis from './components/defis'

function App() {
  
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <div className="card">
        <Defis/>
        </div>
        <button className="change" onClick={() => change()}>Pas celle la ?</button>
      </header>
    </div>
  );
}


function change () {
  window.location.reload();
}

export default App;
